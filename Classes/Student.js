class Student {
  constructor(name, dni) {
    this.name = name;
    this.dni = dni;
    this.subjects = [];
  }
}

module.exports = Student;