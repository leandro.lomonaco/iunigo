class Exam {
    constructor(dni, date, id, grade) {
        this.dni = dni;
        this.date = date;
        this.id = id;
        this.grade = grade;        
    }
}

module.exports = Exam;