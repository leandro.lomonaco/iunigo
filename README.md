### **Requirements:**
* Node.js >10 installed.
* Postman or similar to make requests installed.

### **Instalation:**
* Clone the repository
* Run npm install
* Run npm start

All POST request expect a JSON to be sent on the body.

### **Endpoints:**
All POST request expect a JSON to be sent on the body.

#### Students

* GET  /students --> Gets list of created students.

* POST /students --> Creates a new student.
	###### Parameters:
	* ***string***  - **name**: name
	* ***number*** -  **dni**: identification number  (must have a length of 6 minimum)

Example: 

```json
{
	"name": "Juan Dominguez",
	"dni": 12345678
}
```

#### Subjects

* GET  /subjects --> Gets list of created subjects.

* POST /subjects --> Creates a new subject.

	###### Parameters:
	* ***string***  - **id**: subject id
	* ***string*** -  **subject**: subject name

Example:
```json
{
	"id": "Al",
	"subject": "Algrebra"
}
```

#### Assign Subjects
* GET students/subjects/{dni number} --> Gets the subjects wich the student has signed for.  

* POST /students/sign --> Sign a student in a subject

	###### Parameters:
	* ***string***  - **id**: subject id
	* ***number***  - **dni**: identification number (must have a length of 6 minimum)

Example:
```json
{
	"id": "Al",
	"dni": 123456
}
```

#### Exams

* GET /exams -->Get a list of existing exams

* POST /exams --> Create an exam

	###### Parameters:
	* ***number*** - **dni**: identification number (must have a length of 6 minimum)
	* ***string***  - **date**: date in the format DD-MM-YYYY
	* ***string***  - **subjectId**: subject id
	* ***number***  - **grade**: the grade the student got in the exam
	
Example:
```json
{
	"dni":123456,
	"date": "01-01-2019",
	"subjectId": "Al",
	"grade": 7
}
```

#### Report

* GET /report --> Get a status of the approved subjects for the created students

#### Test

* GET /test --> Creates data for testing pourposes.


