/******** DEPENDENCIES ********/
const express = require('express');
const app = express();
const router = express.Router();
const bodyParser = require("body-parser");




/******** ENDPOINTS ********/
const studentEP = require('./endpoints/Students');
const subjectEP = require('./endpoints/Subjects');
const studentSubjectEP = require('./endpoints/StudentSubjects');
const examEP = require('./endpoints/Exams');
const testEP = require('./endpoints/Test');
const reportEP = require('./endpoints/Report');

/******** CONFIG ********/
const port = 3000
app.use("/", router);
app.use(bodyParser.json());
app.use((err, req, res, next) => {
  //Checks if there is an error in the json
  if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
      return res.sendStatus(400); // Bad request
  }

  next();
});
app.use(studentEP);
app.use(subjectEP);
app.use(studentSubjectEP);
app.use(examEP);
app.use(testEP);
app.use(reportEP);

/******** INITIALIZING SERVER ********/
app.listen(port, () => {
    console.log("listening on " + port);
  });


