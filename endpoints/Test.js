/******** DEPENDENCIES ********/
const app = require('express');
const router = app.Router();
const data = require("../data");
const Subject = require("../Classes/Subject");
const Student = require('../Classes/Student');
const Exam = require('../Classes/Exam');


router.get("/Test", (req, res) => {
    createStudentData();
    createSubjectData();
    assignSubjects();
    createExamData();

    res.status(200).json({
        Message:"Se crearon los datos de prueba",
        Data: data
    });
});

const createStudentData = ()=>{
    data.students = [
        new Student("Pedro Ramirez", 10123456),
        new Student("Joaquin V Gonzalez", 20123456),
        new Student("Guillermo Altolaguirre", 30123456),
        new Student("Mario Romero", 40123456),
        new Student("Ramón Valdez", 50123456)
    ];
}

const createSubjectData = ()=>{
    data.subjects = [
        new Subject("ma", "Matemática"),
        new Subject("le", "Lengua"),
        new Subject("cs", "Ciencias Sociales"),
        new Subject("hi", "Historia"),
        new Subject("co", "Computación")
    ];
}

const assignSubjects = ()=>{
    data.students[0].subjects = [data.subjects[0],data.subjects[1]];
    data.students[1].subjects = [data.subjects[1],data.subjects[2]];
    data.students[2].subjects = [data.subjects[2],data.subjects[3]];
    data.students[3].subjects = [data.subjects[3],data.subjects[4]];
    data.students[4].subjects = [data.subjects[4],data.subjects[0]];
}

const createExamData = ()=>{
    data.exams = [];
    //Casos de prueba fijos
    createExams(data.students[0], data.students[0].subjects[0], [4,8,10]) //Pedro Ramirez pasa el promedio pero tiene un 4 por lo cual desaprueba Matemática.
    createExams(data.students[1], data.students[1].subjects[0], [5,8,8]) //Joaquin V Gonzalez pasa el promedio y ninguna nota baja de 5, aprueba Lengua.
    createExams(data.students[2], data.students[2].subjects[0], [10,4]) // Guillermo Altolaguirre tiene 7 de promedio pero una abajo de 5, desaprueba Ciencias Sociales
    createExams(data.students[3], data.students[3].subjects[0], [6,8]) // Mario Romero pasa el promedio y ninguna abajo de 5, aprueba historia
    createExams(data.students[4], data.students[4].subjects[0], [9,9]) // Ramón Valdezpasa el promedio y ninguna abajo de 5, aprueba computación
    
    //Pruebas al azar
    createRamdonExams(data.students[0]);
    createRamdonExams(data.students[1]);
    createRamdonExams(data.students[2]);
    createRamdonExams(data.students[3]);
    createRamdonExams(data.students[4]);

    
}

const createExams = (student, subject, grades)=>{
    for(let i = 0; i < grades.length; i++){
        data.exams.push(new Exam(student.dni, `01-${i+1}-2020`, subject.id, grades[i]));
    }
}


const createRamdonExams = (student)=>{
    const examQuantity = Math.round(Math.random() * (5 - 1)) + 1;
    //Generamos las notas
    const grades = [];
    for(let i = 0; i < examQuantity; i++){
        grades.push(Math.round(Math.random() * (10 - 3) + 3));
    }
    createExams(student, student.subjects[1], grades);
}
module.exports = router;