/******** DEPENDENCIES ********/
const app = require('express');
const router = app.Router();
const data = require("../data");
const Student = require("../Classes/Student");


/******** ENDPOINTS ********/

//Students list
router.get("/students", (req, res) => {
    res.json(data.students);
});

/***Creates a new student
 * Params: JSON {name,dni}
 */
router.post("/students", (req, res) => {
    const result = createStudent(req.body.name, req.body.dni);
    if(result.ok == true){
        res.status(200).json("Se creo el alumno con éxito");
    }else{
        res.status(400).json(result.message);
    }
    
});


/******** FUNCTIONS ********/

//Validate data
let validateStudent = (name, dni) => {
    let result = {};
    result.ok = true;
    result.message = "ok";

    //Check that no data is missing
    if (typeof name == 'undefined' || name == "") {
        result.ok = false;
        result.message = "Falta el parametro nombre";
    }

    if (typeof dni == 'undefined') {
        result.ok = false;
        result.message = "Falta el parametro dni"
    } else {
        if (dni.length < 6) {
            result.ok = false;
            result.message = "DNI incorrecto";
        }
    }


    //check that not student exists with the given DNI
    if (searchStudent(dni) == true) {
        result.ok = false;
        result.message = "Ya existe un alumno con ese DNI";
    }


    return result;
}

//Searching for duplicates
let searchStudent = (dni) => {
    var filtered = data.students.filter(element => element.dni === dni);
    if (filtered.length > 0) {
        return true;
    } else {
        return false;
    }
}

//create students if validation is ok
let createStudent = (name, dni) => {
    const result = validateStudent(name, dni);
    if (result.ok == true) {
        data.students.push(new Student(name, dni));
    }
    return result;
}

module.exports = router;