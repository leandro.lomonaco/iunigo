/******** DEPENDENCIES ********/
const app = require('express');
const router = app.Router();
const data = require("../data");


//Create Report
router.get("/report", (req, res) => {
    const result = createReport();
    res.status(200).json(result);
});



const createReport = ()=>{
    let report = {};
    report.student = [];

    //Get students data
    data.students.forEach(student => {
            //create object to add to the report
            const studentData = {};
            //set name
            studentData.name = student.name;
            studentData.subjects = [];
           

            //Get subject data
            student.subjects.forEach(subject =>{
                //create the subject object
                subjectData = {}
                subjectData.name = subject.name;
                subjectData.approved = checkIfAproved(student.dni, subject.id);
                studentData.subjects.push(subjectData);
                
            })
                        
            //add object to the report
            report.student.push(studentData);
    })
    return report;
}

const checkIfAproved = (dni, subjectID) => {
    let approved = "";

    //get exams for the given dni and subject id
    let exams = data.exams.filter(element => element.dni == dni && element.id == subjectID);
    
    //if it got no exams its automatically disapproved
    if(exams.length < 1){
        approved = false;
    }else{
        //check if it got some grade below five
        const disapproved = exams.find(element => element.grade < 5);
        
        if(disapproved){
            approved = false;
        }else{
            //get the average of all grades
            let sum = 0;
            let count = 0;
            exams.forEach(exam => {
                sum += exam.grade;
                count++
            })
            if(sum/count >= 7){
                approved = true
            }else{
                approved = false;
            }
        }
    }
    if(approved){
        return "Aprobado"
    }else{
        return "Desaprobado"
    }
}

module.exports = router;