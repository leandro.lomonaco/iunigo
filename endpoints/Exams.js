/******** DEPENDENCIES ********/
const app = require('express');
const router = app.Router();
const data = require("../data");
const Exam = require("../Classes/Exam");


/******** ENDPOINTS ********/

//Exam list
router.get("/exams", (req, res) => {
    res.status(200).json(data.exams);
});

/***Creates a new exam
 * Params: JSON {integer: dni, string(DD-MM-YYYY): date , string: subjectId, integer: grade}
 */
router.post("/Exams", (req, res) => {
    const result = createExam(req.body.dni, req.body.date, req.body.subjectId, req.body.grade);
    if (result.ok == true) {
        res.status(200).json("Se creo el examen con éxito");
    } else {
        res.status(400).json(result.message);
    }
});


/******** FUNCTIONS ********/

//Validate data
let validate = (dni, date, subjectId, grade) => {
    let result = {};
    result.ok = true;
    result.message = "ok";

    //we check that there is no missing data
    if (typeof dni == 'undefined' || dni == "") {
        result.ok = false;
        result.message = "El dni no puede estar vacio";
    } else if (typeof date == 'undefined' || date == "") {
        result.ok = false;
        result.message = "La fecha no puede estar vacía";
    } else if (typeof subjectId == 'undefined' || subjectId == "") {
        result.ok = false;
        result.message = "El Id de la materia no puede estar vacio";
    } else if (typeof grade == 'undefined') {
        result.ok = false;
        result.message = "La nota no puede estar vacia";
    }else if (typeof grade != 'number'){
        result.ok = false;
        result.message = "El formato de la nota debe ser numero";
    } else if(grade > 10 || grade < 1){
        result.ok = false;
        result.message = "La nota debe ser del 1 al 10";
    }

    if (result.ok) {
        //Now we must check that the student and subject exists and the student is signed in that subject.
        const student = data.students.find(element => element.dni == dni);
        const subject = data.subjects.find(element => element.id == subjectId);
        //if student exist we check subject
        if (student) {
            //if subject exist we check if the student is signed
            if (subject) {
                const signedSubject = student.subjects.find(element => element.id == subjectId)
                if (!signedSubject) {
                    result.ok = false;
                    result.message = "El alumno no se anotó a esta materia";
                }
            } else {
                result.ok = false;
                result.message = "La materia no existe";
            }
        } else {
            result.ok = false;
            result.message = "El alumno no existe";
        }
    }

    return result;
}


//create subject if validation is ok
let createExam = (dni, date, subjectId, grade) => {
    const result = validate(dni, date, subjectId, grade);
    if (result.ok == true) {
        data.exams.push(new Exam(dni, date, subjectId, grade));
    }
    return result;
}

module.exports = router;





