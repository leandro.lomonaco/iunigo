/******** DEPENDENCIES ********/
const app = require('express');
const router = app.Router();
const data = require("../data");

/******** ENDPOINTS ********/

/***Search for the subjects which the student has signed in
 * Params: dni
 */
router.get("/students/subjects/:dni", (req, res) => {

    const student = data.students.find(element => element.dni == req.params.dni)
    if (typeof student == "undefined") {
        res.status(400).json("No hay alumnos con ese DNI");
    } else {
        res.status(200).json(student.subjects);
    }
});


/***Signs a student to the subject
 * Params: JSON {dni,id}
 */
router.post("/students/sign/", (req, res) => {
    const result = signStudent(req.body.dni, req.body.id);
    if (result.ok == true) {
        res.status(200).json("Se asigno la materia con éxito");
    } else {
        res.status(400).json(result.message);
    }

});

/******** FUNCTIONS ********/

//Validate data
let validateSigning = (dni, id) => {
    let result = {};
    result.ok = true;
    result.message = "ok";

    //Check that no data is missing
    if (typeof id == 'undefined' || id == "") {
        result.ok = false;
        result.message = "Falta el parametro id";
    }
    if (typeof dni == 'undefined' || dni == "") {
        result.ok = false;
        result.message = "Falta el parametro dni";
    }


    if (result.ok == true) {
        const student = data.students.find(element => element.dni == dni);
        const subject = data.subjects.find(element => element.id == id);

        //Check that both student and subject exists
        if (typeof student == "undefined") {
            result.ok = false;
            result.message = "El alumno no existe";
        } else if (typeof subject == "undefined") {
            result.ok = false;
            result.message = "La materia no existe";
        } else {
            //check that student is not already signed for the subject
            if (searchSigned(student, subject) === true) {
                result.ok = false;
                result.message = "El alumno ya está anotado en esa materia";
            } else {
                result.student = student;
                result.subject = subject;
            }
        }
    }
    return result;
}

//if it pass validations, signs the student into the subject
let signStudent = (dni, id) => {
    const result = validateSigning(dni, id);
    if (result.ok == true) {
        result.student.subjects.push(result.subject);
    }
    return result;

}

//Searching for duplicates
let searchSigned = (student, subject) => {
    var filtered = student.subjects.filter(element => element.id == subject.id);
    if (filtered.length > 0) {
        return true;
    } else {
        return false;
    }
}


module.exports = router;