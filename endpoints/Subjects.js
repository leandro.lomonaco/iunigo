/******** DEPENDENCIES ********/
const app = require('express');
const router = app.Router();
const data = require("../data");
const Subject = require("../Classes/Subject");


/******** ENDPOINTS ********/

//Subjects list
router.get("/subjects", (req, res) => {
    res.status(200).json(data.subjects);
});

/***Signs a student to the subject
 * Params: JSON {id,subject}
 */
router.post("/subjects", (req, res) => {
    const result = createSubject(req.body.id, req.body.subject);
    if(result.ok == true){
        res.status(200).json("Se creo la materia con éxito");
    }else{
        res.status(400).json(result.message);
    }
});


/******** FUNCTIONS ********/

//Validate data
let validate = (id, subject) => {
    let result = {};
    result.ok = true;
    result.message = "ok";
    
    //we check that there is no missing data
    if (typeof subject == 'undefined' || subject == "") {
        result.ok = false;
        result.message = "El nombre de la materia no puede estar vacio";
    }

    if (typeof id == 'undefined' || id == "") {
        result.ok = false;
        result.message = "El id no puede estar vacio";
    }

    //we check that a subject with the same id does not exist.
    if (searchSubject(id) == true) {
        result.ok = false;
        result.message = "Ya existe una materia con ese ID";
    }

    return result;
}


//Searching for duplicates
let searchSubject = (id) => {
    var filtered = data.subjects.filter(element => element.id === id);
    if (filtered.length > 0) {
        return true;
    } else {
        return false;
    }
}



//create subject if validation is ok
let createSubject = (id, subject) => {
    const result = validate(id, subject);
    if(result.ok == true){
        data.subjects.push(new Subject(id, subject));
    }
    return result;
}

module.exports = router;





